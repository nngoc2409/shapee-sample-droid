package com.shapee.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.shapee.util.Config

@Entity(tableName = "Media")
class Media {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0

    @ColumnInfo(name = "org_path")
    var orgPath: String = ""

    @ColumnInfo(name = "current_path")
    var path: String = ""

    @ColumnInfo(name = "fileName")
    var fileName: String = ""

    @ColumnInfo(name = "mime_type")
    var mimeType: String = ""

    @ColumnInfo(name = "extension")
    var extension: String = ""

    @ColumnInfo(name = "encodedExtension")
    var encodedExtension: String = Config.DEFAULT_ENCODE_FILE_EXTENSION

    @ColumnInfo(name = "encodedParam")
    var encodedParam: String = Config.DEFAULT_ENCODE_PARAM

    @ColumnInfo(name = "created_date")
    var createdDate: Long = 0

    @ColumnInfo(name = "org_added_date")
    var orgAddedDate: Long = 0

    @ColumnInfo(name = "org_taken_date")
    var orgTakenDate: Long = 0
}