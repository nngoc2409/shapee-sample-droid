package com.shapee.database

import android.content.Context
import android.os.Environment
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.shapee.BuildConfig
import com.shapee.database.dao.MediaDAO
import com.shapee.database.entity.Media
import java.io.File
import android.os.AsyncTask
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Media::class], version = 1, exportSchema = false)
abstract class PassCodeDatabase : RoomDatabase() {
    abstract fun mediaDao(): MediaDAO

    companion object {

        @Volatile
        private var INSTANCE: PassCodeDatabase? = null

        fun getInstance(context: Context): PassCodeDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): PassCodeDatabase {
            val folderPath = BuildConfig.APPLICATION_ID
            val folder = File(Environment.getExternalStorageDirectory(), folderPath)
            folder.mkdirs()

            return Room.databaseBuilder(
                context.applicationContext,
                PassCodeDatabase::class.java, File(folder, "PassCodeDatabase").path
            ).build()

        }
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     *
     * If you want to populate the database only when the database is created for the 1st time,
     * override RoomDatabase.Callback()#onCreate
     */
    private val sRoomDatabaseCallback = object : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            // If you want to keep the data through app restarts,
            // comment out the following line.
            INSTANCE?.let { PopulateDbAsync(it).execute() }
        }
    }

    /**
     * Populate the database in the background.
     * If you want to start with more words, just add them.
     */
    private class PopulateDbAsync internal constructor(db: PassCodeDatabase) : AsyncTask<Void, Void, Void>() {

        private val mDao: MediaDAO

        init {
            mDao = db.mediaDao()
        }

        override fun doInBackground(vararg params: Void): Void? {

            return null
        }
    }
}