package com.shapee.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.shapee.database.entity.Media

@Dao
interface MediaDAO {
    // Insert Media to DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(media: Media): Long
    // QGet all Media from DB
    @Query("SELECT * FROM media ORDER BY created_date DESC")
    fun getAll(): List<Media>
    // Get all Media from DB
    @Query("SELECT * FROM media ORDER BY created_date DESC")
    fun getAllAsLiveData(): LiveData<List<Media>>
    // Delete Media in DB
    @Delete
    fun delete(media: Media): Int
    //Delete Media by id in DB
    @Query("DELETE FROM media WHERE id=:id")
    fun delete(id: Int): Int

}