package com.shapee.util

import android.content.Context
import android.media.MediaScannerConnection
import android.os.Environment
import android.util.DisplayMetrics
import com.shapee.database.PassCodeDatabase
import com.shapee.database.entity.Media
import com.shapee.feature.gallery.model.GalleryItem
import io.reactivex.Observable
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


object Util {
    private var DATE_FORMATTER = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun encodeFiles(context: Context, items: Array<GalleryItem>): Observable<ArrayList<Media>> {
        return Observable.fromCallable {
            val db = PassCodeDatabase.getInstance(context)
            val mediaItems = ArrayList<Media>()
            val fileNameOrg = DATE_FORMATTER.format(Date())
            items.forEach {
                val newFileName = fileNameOrg.plus("_").plus(items.indexOf(it) + 1)

                val folder = File(Environment.getExternalStorageDirectory(), Config.IMAGE_FOLDER_NAME)
                folder.mkdirs()
                val file = File(it.path)
                val destinationFile =
                    File(folder, newFileName.plus(".").plus(Config.DEFAULT_ENCODE_FILE_EXTENSION))

                file.renameTo(destinationFile)
                try {
                    FileUtil.changeAccessFile(destinationFile.absolutePath, Config.DEFAULT_ENCODE_PARAM)
                } catch (e: Throwable) {
                    destinationFile.renameTo(file)
                    throw e
                }
                val mediaItem = Media().apply {
                    fileName = newFileName
                    extension = StringUtil.getFileExtension(file.name) ?: ""
                    encodedExtension = Config.DEFAULT_ENCODE_FILE_EXTENSION
                    orgPath = it.path
                    path = File(folder, newFileName).path
                    mimeType = it.mimeType
                    orgAddedDate = it.addedDate
                    orgTakenDate = it.takenDate
                }
                mediaItem.id = db.mediaDao().insert(mediaItem)

                mediaItems.add(mediaItem)

                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(it.path), null, null
                )

            }
            mediaItems
        }
    }

    fun restoreFiles(context: Context, items: Array<Media>): Observable<ArrayList<Media>> {
        return Observable.fromCallable {
            val db = PassCodeDatabase.getInstance(context)
            val mediaItems = ArrayList<Media>()
            items.forEach {
                val encodedFile = File(it.path.plus(".").plus(it.encodedExtension))
                if (encodedFile.exists()) {
                    decodeFile(it)
                }
                val file = getEncodedFile(it)
                if (file.exists()) {
                    val destinationFile = File(it.orgPath)
                    file.renameTo(destinationFile)

                    mediaItems.add(it)

                    MediaScannerConnection.scanFile(
                        context,
                        arrayOf(destinationFile.path), null, null
                    )
                }
                db.mediaDao().delete(it)

            }
            mediaItems
        }
    }

    fun encodeFiles(items: List<Media>): Observable<ArrayList<Media>> {
        return Observable.fromCallable {
            val encodedItems = ArrayList<Media>()
            items.forEach {
                val file = getEncodedFile(it)
                if (file.exists()) {
                    val destinationFile = File(it.path.plus(".").plus(it.encodedExtension))
                    file.renameTo(destinationFile)
                    try {
                        FileUtil.changeAccessFile(destinationFile.absolutePath, Config.DEFAULT_ENCODE_PARAM)
                    } catch (e: Throwable) {
                        destinationFile.renameTo(file)
                        throw e
                    }
                }
                encodedItems.add(it)
            }
            encodedItems
        }
    }

    fun decodeFiles(items: ArrayList<Media>): Observable<ArrayList<Media>> {
        return Observable.fromCallable {
            val encodedItems = ArrayList<Media>()
            items.forEach {
                decodeFile(it)
                encodedItems.add(it)
            }
            encodedItems
        }
    }

    fun decodeFile(item: Media): File {
        val file = File(item.path.plus(".").plus(item.encodedExtension))
        val destinationFile = getEncodedFile(item)

        if (file.exists()) {
            file.renameTo(destinationFile)
            try {
                FileUtil.changeAccessFile(destinationFile.absolutePath, Config.DEFAULT_ENCODE_PARAM)
            } catch (e: Throwable) {
                destinationFile.renameTo(file)
                throw e
            }
        }
        return destinationFile
    }

    fun getEncodedFile(item: Media): File = File(item.path)
}