package com.shapee.util

import com.shapee.BuildConfig

object Config {
    const val DEFAULT_ENCODE_PARAM = BuildConfig.APPLICATION_ID
    const val DEFAULT_ENCODE_FILE_EXTENSION = "encoded"
    const val IMAGE_FOLDER_NAME = BuildConfig.APPLICATION_ID.plus("/images")
    const val NUM_BYTE_ENCODE = 128L
}
