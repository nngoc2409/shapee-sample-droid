package com.shapee.util

object StringUtil {
    fun getFileExtension(fileName: String): String? =
        if (fileName.contains('.') && fileName.lastIndexOf('.') != fileName.length - 1) fileName.substring(
            fileName.lastIndexOf(
                '.'
            ) + 1
        ) else null

}