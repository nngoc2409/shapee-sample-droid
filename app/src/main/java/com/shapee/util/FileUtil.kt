package com.shapee.util

import io.reactivex.Observable
import java.io.File
import java.io.RandomAccessFile
import kotlin.experimental.xor

object FileUtil {
    private fun getByte(paramArrayOfByte: ByteArray, encodeString: String): ByteArray {
        val encodeByteArray = encodeString.toByteArray()
        val arrayOfByte = paramArrayOfByte.clone()
        val byteToReplace = if (arrayOfByte.size < encodeByteArray.size)
            arrayOfByte.size
        else
            encodeByteArray.size
        for (i in 0 until byteToReplace) {
            arrayOfByte[i] = (arrayOfByte[i] xor encodeByteArray[i])
        }

        return arrayOfByte
    }

    fun changeAccessFile(filePath: String, encodeString: String): Boolean {
        try {
            val file = File(filePath)
            val localRandomAccessFile = RandomAccessFile(
                file,
                "rw"
            )
            val numByte = Math.min(Config.NUM_BYTE_ENCODE, file.length())
            val arrayOfByte = ByteArray(numByte.toInt())
            localRandomAccessFile.read(arrayOfByte, 0, arrayOfByte.size)
            localRandomAccessFile.seek(0L)
            if (localRandomAccessFile.length() > arrayOfByte.size) {
                localRandomAccessFile.write(getByte(arrayOfByte, encodeString))
            }
            localRandomAccessFile.close()
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    fun encodeFile(sourceFile:String, encodeString: String):Observable<String>{
        return Observable.fromCallable {
            changeAccessFile(sourceFile, encodeString)
            sourceFile
        }
    }
}