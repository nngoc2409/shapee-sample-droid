package com.shapee.base
import androidx.lifecycle.LifecycleOwner

interface IBaseActivity : LifecycleOwner {

    fun dismissLoading()

    fun showLoading()

    fun addFragment(layout: Int, fragment: BaseFragment<*>, name: String)

    fun replaceFragment(layout: Int, fragment: BaseFragment<*>, name: String)

    fun showToast(message: String)

    fun showError(throwable: Throwable)

    fun showAlert(message: String)

    fun hideKeyBoard()

}
