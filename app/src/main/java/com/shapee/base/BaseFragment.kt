package com.shapee.base

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment

abstract class BaseFragment<BP : BasePresenter> : Fragment {
    var mContext: Context? = null

    protected var mPresenter: BP? = null

    constructor()

    constructor(context: Context) {
        this.mContext = context
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        mPresenter = getPresenter()
    }
    // Show Loading
    fun showLoading() {
        if (mContext != null && activity != null && !activity!!.isFinishing) {
            (mContext as IBaseActivity).showLoading()
        }
    }
    // Dismiss Loading
    fun dismissLoading() {
        if (mContext != null && activity != null && !activity!!.isFinishing) {
            (mContext as IBaseActivity).dismissLoading()
        }
    }
    // Show error exception
    fun showError(throwable: Throwable) {
        if (mContext != null && activity != null && !activity!!.isFinishing) {
            (mContext as BaseActivity<*>).showError(throwable)
        }
    }

    // Show alert with Message
    fun showAlert(msg: String) {
       if (mContext != null && activity != null && !activity!!.isFinishing)
           (mContext as BaseActivity<*>).showAlert(msg)
    }


    protected abstract fun getPresenter(): BP?

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter?.onCleared()
    }
}
