package com.shapee.base

import android.Manifest
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.shapee.R

abstract class BaseActivity<BP : BasePresenter> : AppCompatActivity(), IBaseActivity {
    protected var mPresenter: BP? = null
    protected var mLoadingDialog: ProgressDialog? = null
    protected var mAlertDialog: Dialog? = null
    protected var mToast: Toast? = null
    protected var mCheckPermissionDelegate: CheckPermissionDelegate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = getPresenter()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.onCleared()
    }

    protected abstract fun getPresenter(): BP?
    // Show alert with Message
    override fun showAlert(message: String) {
        mAlertDialog?.dismiss()
        mAlertDialog = AlertDialog.Builder(this).apply {
            setMessage(message)
            setPositiveButton(R.string.ok, null)
        }.show()
    }
    // Show Loading
    override fun showLoading() {
        mLoadingDialog = ProgressDialog(this);
        mLoadingDialog?.setMessage(getString(R.string.loading))
        if (mLoadingDialog?.isShowing != true)
            mLoadingDialog?.show()
    }
    // Dismiss Loading
    override fun dismissLoading() {
        mLoadingDialog?.dismiss()
    }
    // Add Fragment
    override fun addFragment(layout: Int, fragment: BaseFragment<*>, name: String) {
        supportFragmentManager.beginTransaction().add(layout, fragment).addToBackStack(name).commitAllowingStateLoss()
    }
    // Replace Fragment
    override fun replaceFragment(layout: Int, fragment: BaseFragment<*>, name: String) {
        supportFragmentManager.beginTransaction().replace(layout, fragment).addToBackStack(name)
            .commitAllowingStateLoss()
    }
    // Show Toast
    override fun showToast(message: String) {
        mToast?.cancel()
        mToast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        mToast?.show()
    }
    // Show error exception
    override fun showError(throwable: Throwable) {
        mAlertDialog?.dismiss()
        mAlertDialog = AlertDialog.Builder(this).apply {
            setMessage(throwable.message)
            setPositiveButton(R.string.ok, null)
        }.show()
    }
    // Hide keyboard
    override fun hideKeyBoard() {
        runOnUiThread {
            this@BaseActivity.currentFocus?.let {
                try {
                    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        it.applicationWindowToken, InputMethodManager.HIDE_NOT_ALWAYS
                    )
                } catch (e: IllegalStateException) {
                } catch (e: Exception) {
                }
            }
        }
    }
    // Check permission
    fun checkPermission(permissions: Array<String>, delegate: CheckPermissionDelegate?): Boolean {
        if (permissions.any {
                ActivityCompat.checkSelfPermission(
                    this@BaseActivity,
                    it
                ) != PackageManager.PERMISSION_GRANTED
            }) {
            ActivityCompat.requestPermissions(this, permissions, 10)
        }

        delegate?.onSuccess()
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.any { it != PackageManager.PERMISSION_GRANTED }) {
            mCheckPermissionDelegate?.onError()
        } else {
            mCheckPermissionDelegate?.onSuccess()
        }
    }

    interface CheckPermissionDelegate {
        fun onSuccess()
        fun onError()
    }
}