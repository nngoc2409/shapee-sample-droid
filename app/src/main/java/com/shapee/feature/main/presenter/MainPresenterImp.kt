package com.shapee.feature.main.presenter

import androidx.lifecycle.LiveData
import com.shapee.base.BasePresenter
import com.shapee.database.PassCodeDatabase
import com.shapee.database.entity.Media
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.feature.main.view.MainView
import com.shapee.util.Util
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainPresenterImp(private val mView: MainView) : BasePresenter(), MainPresenter {

    override fun encodeFile(items: ArrayList<GalleryItem>) {
        mView.apply {
            Util.encodeFiles(mView.getActivityContext(), items.toTypedArray())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, { mView.getActivityContext().showError(it) })

        }
    }

    override fun loadData() :LiveData<List<Media>>{
        return PassCodeDatabase.getInstance(mView.getActivityContext()).mediaDao().getAllAsLiveData()
    }
}