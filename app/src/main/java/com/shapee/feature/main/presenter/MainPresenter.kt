package com.shapee.feature.main.presenter

import androidx.lifecycle.LiveData
import com.shapee.database.entity.Media
import com.shapee.feature.gallery.model.GalleryItem

interface MainPresenter {
    fun encodeFile(items: ArrayList<GalleryItem>)
    fun loadData():LiveData<List<Media>>
}