package com.shapee.feature.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shapee.R
import com.shapee.database.entity.Media
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.util.Util
import kotlinx.android.synthetic.main.item_gallery.view.*
import java.io.File

class MediaAdapter(var data: List<Media>, val listener: OnItemSelectedChangedListener?) :
    RecyclerView.Adapter<MediaAdapter.ViewHolder>() {

    val selectedItems = ArrayList<Media>()

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_gallery, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        val file = File(item.path.plus(".").plus(item.encodedExtension))
        if(file.exists()){
            Util.decodeFile(item)
        }
        // Display photo on list
        Glide.with(holder.itemView.context).load(Util.getEncodedFile(item)).into(holder.itemView.imageView)
        holder.itemView.isSelected = selectedItems.contains(item)
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            // Action click on item
            view.setOnClickListener {
                val item = data[layoutPosition]
                if(selectedItems.contains(item)){
                    selectedItems.remove(item)
                }else{
                    selectedItems.add(item)
                }
                notifyItemChanged(layoutPosition)
                listener?.onItemSelectedChanged(selectedItems, item)
            }
        }
    }

    interface OnItemSelectedChangedListener {
        fun onItemSelectedChanged(selectedItem: ArrayList<Media>, item: Media)
    }

}