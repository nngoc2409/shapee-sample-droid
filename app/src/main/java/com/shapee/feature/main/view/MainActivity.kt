package com.shapee.feature.main.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.shapee.R
import com.shapee.base.BaseActivity
import com.shapee.database.entity.Media
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.feature.gallery.view.GalleryActivity
import com.shapee.feature.main.adapter.MediaAdapter
import com.shapee.feature.main.presenter.MainPresenterImp
import com.shapee.util.Util
import com.shapee.view.widget.GridSpacingItemDecoration
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainPresenterImp>(), MainView {
    private var mAdapter: MediaAdapter? = null

    override fun getPresenter(): MainPresenterImp? {
        return MainPresenterImp(this)
    }

    override fun getActivityContext(): MainActivity {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Check Permission
        checkPermission(
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
            object : CheckPermissionDelegate {
                override fun onError() {
                    finish()
                }

                override fun onSuccess() {
                    mPresenter?.let {
                        showData(it.loadData())
                    }

                }
            })
        flbAdd.setOnClickListener {
            startActivityForResult(Intent(this@MainActivity, GalleryActivity::class.java), 100)
        }
        //Load all image on UI
        mAdapter = MediaAdapter(ArrayList(), object : MediaAdapter.OnItemSelectedChangedListener {
            override fun onItemSelectedChanged(selectedItem: ArrayList<Media>, item: Media) {

            }
        })
        rcMedia.layoutManager = GridLayoutManager(this, 4)
        rcMedia.adapter = mAdapter
        rcMedia.addItemDecoration(
            GridSpacingItemDecoration(
                resources.getDimensionPixelSize(R.dimen.gallery_item_padding),
                false
            )
        )
    }

    override fun showData(data: LiveData<List<Media>>) {
        data.observe(this, Observer {
            mAdapter?.data = it
            mAdapter?.notifyDataSetChanged()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val items = data?.getSerializableExtra(GalleryActivity.EXTRA_DATA) as ArrayList<GalleryItem>?
            items?.let {
                mPresenter?.encodeFile(it)
            }
        }
    }

    override fun onDestroy() {
        mAdapter?.let {
            if(it.itemCount> 0){
                Util.encodeFiles(it.data).subscribeOn(Schedulers.io()).subscribe()
            }
        }

        super.onDestroy()
    }
}
