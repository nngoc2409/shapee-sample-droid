package com.shapee.feature.main.view

import androidx.lifecycle.LiveData
import com.shapee.database.entity.Media

interface MainView{
    fun getActivityContext():MainActivity

    fun showData(data:LiveData<List<Media>>)
}
