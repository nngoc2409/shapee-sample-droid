package com.shapee.feature.gallery.presenter

import android.provider.MediaStore
import com.shapee.base.BasePresenter
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.feature.gallery.view.GalleryView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GalleryPresenterImp(private val mView: GalleryView) : BasePresenter(), GalleryPresenter {
    // Get all image in Gallery
    override fun loadImages() {
        mView.apply {
            getActivityContext().apply {
                Observable.fromCallable {
                    val projection = arrayOf(
                        MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                        MediaStore.Images.Media.DATA,
                        MediaStore.Images.Media.MIME_TYPE,
                        MediaStore.Images.Media.DATE_TAKEN,
                        MediaStore.Images.Media.DATE_ADDED
                    )
                    // content: style URI for the "primary" external storage volume
                    val images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

                    // Make the query.
                    val cursor = getActivityContext().contentResolver.query(
                        images, projection,
                        null, null, MediaStore.Images.Media.DATE_ADDED + " DESC"
                    )

                    val items = ArrayList<GalleryItem>()

                    cursor?.let {
                        if (it.moveToFirst()) {
                            val bucketNameColumn = it.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
                            val imageUriColumn = it.getColumnIndex(MediaStore.Images.Media.DATA)
                            val imageIdColumn = it.getColumnIndex(MediaStore.Images.Media._ID)
                            val mimeTypeColumn = it.getColumnIndex(MediaStore.Images.Media.MIME_TYPE)
                            val takenDateColumn = it.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN)
                            val addedDateColumn = it.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)

                            do {
                                items.add(
                                    GalleryItem(
                                        it.getInt(imageIdColumn),
                                        it.getString(bucketNameColumn),
                                        it.getString(imageUriColumn),
                                        it.getString(mimeTypeColumn),
                                        it.getLong(takenDateColumn),
                                        it.getLong(addedDateColumn)
                                    )
                                )
                            } while (cursor.moveToNext())
                        }

                    }
                    cursor?.close()
                    items
                }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

                    .doOnSubscribe {

                    }.doFinally {

                    }.subscribe({
                        onGetAlbumSuccess(it)
                    }, { getActivityContext().showError(it) })

            }
        }
    }
}