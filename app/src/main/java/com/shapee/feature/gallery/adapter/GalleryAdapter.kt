package com.shapee.feature.gallery.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shapee.R
import com.shapee.feature.gallery.model.GalleryItem
import kotlinx.android.synthetic.main.item_gallery.view.*
import java.io.File

class GalleryAdapter(var data: ArrayList<GalleryItem>, val listener: OnItemSelectedChangedListener?) :
    RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    val selectedItems = ArrayList<GalleryItem>()

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_gallery, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        // Display photo on list
        Glide.with(holder.itemView.context).load(File(item.path)).into(holder.itemView.imageView)
        holder.itemView.isSelected = selectedItems.contains(item)
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            // Action click on item
            view.setOnClickListener {
                val item = data[layoutPosition]
                if (selectedItems.contains(item)) {
                    selectedItems.remove(item)
                } else {
                    selectedItems.add(item)
                }
                notifyItemChanged(layoutPosition)
                listener?.onItemSelectedChanged(selectedItems, item)
            }
        }
    }

    interface OnItemSelectedChangedListener {
        fun onItemSelectedChanged(selectedItem: ArrayList<GalleryItem>, item: GalleryItem)
    }
}