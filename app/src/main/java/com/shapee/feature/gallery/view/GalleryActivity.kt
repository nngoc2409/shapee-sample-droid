package com.shapee.feature.gallery.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import com.shapee.R
import com.shapee.base.BaseActivity
import com.shapee.feature.gallery.adapter.GalleryAdapter
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.feature.gallery.presenter.GalleryPresenterImp
import com.shapee.view.widget.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.activity_gallery.*

class GalleryActivity : BaseActivity<GalleryPresenterImp>(), GalleryView {
    companion object {
        const val EXTRA_DATA = "data"
    }

    var mAdapter: GalleryAdapter? = null

    override fun getPresenter(): GalleryPresenterImp? {
        return GalleryPresenterImp(this)
    }

    override fun getActivityContext(): BaseActivity<GalleryPresenterImp> {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        toolbar.setNavigationOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        //Load all image on UI
        mAdapter = GalleryAdapter(ArrayList(), object : GalleryAdapter.OnItemSelectedChangedListener {
            override fun onItemSelectedChanged(selectedItem: ArrayList<GalleryItem>, item: GalleryItem) {

            }
        })
        rcImage.layoutManager = GridLayoutManager(this, 4)
        rcImage.adapter = mAdapter
        rcImage.addItemDecoration(
            GridSpacingItemDecoration(
                resources.getDimensionPixelSize(R.dimen.gallery_item_padding),
                false
            )
        )

        mPresenter?.loadImages()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_gallery, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_done -> {
                if(mAdapter?.selectedItems?.isNullOrEmpty() != false){
                    showAlert(getString(R.string.msg_choose_item_gallery))
                }else{
                    setResult(Activity.RESULT_OK, Intent().apply {
                        putExtra(EXTRA_DATA, mAdapter?.selectedItems)
                    })
                    finish()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onGetAlbumSuccess(items: ArrayList<GalleryItem>) {
        mAdapter?.let {
            it.data.addAll(items)
            it.notifyDataSetChanged()
        }
    }

    override fun addItem(item: GalleryItem) {
        mAdapter?.let {
            it.data.add(item)
            it.notifyItemInserted(it.data.indexOf(item))
        }
    }
}