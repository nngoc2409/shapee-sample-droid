package com.shapee.feature.gallery.model

import java.io.Serializable

data class GalleryItem(
    var id: Int,
    var name:String,
    var path: String,
    var mimeType:String,
    var takenDate:Long,
    var addedDate:Long):Serializable