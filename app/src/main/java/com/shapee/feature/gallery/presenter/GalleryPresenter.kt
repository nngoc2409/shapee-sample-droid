package com.shapee.feature.gallery.presenter

interface GalleryPresenter{
    fun loadImages()
}