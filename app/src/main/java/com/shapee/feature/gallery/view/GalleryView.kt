package com.shapee.feature.gallery.view

import com.shapee.base.BaseActivity
import com.shapee.feature.gallery.model.GalleryItem
import com.shapee.feature.gallery.presenter.GalleryPresenterImp

interface GalleryView {
    fun getActivityContext(): BaseActivity<GalleryPresenterImp>

    fun onGetAlbumSuccess(items: ArrayList<GalleryItem>)

    fun addItem(item: GalleryItem)
}