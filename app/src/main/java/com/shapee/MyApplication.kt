package com.shapee

import androidx.multidex.MultiDexApplication
import com.shapee.database.PassCodeDatabase
import com.shapee.util.Util
import io.reactivex.schedulers.Schedulers

class MyApplication : MultiDexApplication() {
    companion object {
        var instance: MyApplication? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun onTerminate() {

        super.onTerminate()
    }

}